# File-System

## Notas:
1. O programa funciona estilo o ui do file system do windows em que o user tem de navegar até á pasta.
2. Funcionalidades como mover e copiar diretorios/ficheiros e verificar os seus tamanhos não ficaram implementadas.
3. O projeto não contém persistência.
4. Ao executar o programa, é criado um ficheiro root na pasta do projeto onde se pode ver o programa apesar de ser object oriented
   também cria os ficheiros no systema.
5. Obrigado por me darem a oportunidade de desenvolver este desafio!

## Diagrama  UML da solução desenvolvida
![Diagrama_UML](Diagrama/Diagrama_UML.png)

## Como Executar
1. Executar cmd e utilizar cd até ao diretorio do ficheiro. (..\FileSystem\FileSystem)
2. Correr na pasta **java -jar FileSystem-1.0-SNAPSHOT.jar**

### Commandos do file system

* **ls** verifica conteudos de um diretorio.

* **cd <folder_name>** move file system para o proximo diretorio com <folder_name> do diretorio corrente se existir.

* **rm <folder_name>** remove o diretorio ou ficheiro com <folder_name> do diretorio corrente se existir.

* **rmFile <file_name>** remove o ficheiro com <folder_name> do diretorio corrente se existir.

* **makedir <folder_name>** cria um diretorio no diretorio corrente com o nome <folder_name>.

* **makefile <file_name.extension>** cria um ficheiro com a extensao pretendida no diretorio corrente com o nome e extensão <folder_name.extension>.

* **nano <file_name.extension>** lê o conteudo do ficheiro no diretorio corrente com o nome <file_name.extension> introduzido e mostra um promt para escrever novo conteudo no ficheiro.

* **cat <file_name.extension>** mostra o conteudo do ficheiro no diretorio corrente com o nome <folder_name>.

* **chkCreatedDate <folder_name OR file_name.extension>** mostra a data de criacao de um ficheiro ou diretorio no diretorio corrente com <folder_name OR file_name.extension>.

* **chkUpdatedDate <folder_name OR file_name.extension>** mostra a data da ultima atualização de um ficheiro ou diretorio no diretorio corrente com <folder_name OR file_name.extension>.

* **options** mostra o menu

* **q** termina o programa (Utilizar sempre que sair do programa senão é necessário remover manualmente a pasta root da pasta d eprojeto)





