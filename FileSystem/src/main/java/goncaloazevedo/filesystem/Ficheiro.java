/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package goncaloazevedo.filesystem;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Goncalo Azevedo
 */
public class Ficheiro extends Item{
    
    File ficheiro;
    private String content;
    private String typeOfFile;

    public Ficheiro(String content, String name,String currentPath, Diretorio directory) throws IOException {
        
        super(name, currentPath, directory);
        this.content = content;
        this.typeOfFile = this.name.split("\\.")[1];
        this.ficheiro = new File(currentPath + "\\" + name);
        this.ficheiro.createNewFile();
        
    }
    
    public void delete(){
        ficheiro.delete();
    }

    /**
     * Retuns the content of the selected file
     * @return content of the file
     */
    public String getContent() {
        return content;
    }

    /**
     * Updated the content of the selected file
     * @param content new content to be written into the selected file
     * @throws IOException
     */
    public void setContent(String content) throws IOException {
        this.content = content;
        
        BufferedWriter writer = new BufferedWriter(new FileWriter(ficheiro));
        writer.write(content);
        writer.close();
        
        System.out.println("Content updated");
    }
    
}
