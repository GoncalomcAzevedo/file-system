/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package goncaloazevedo.filesystem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Goncalo Azevedo
 */
public class Item {
    
    //Permissoes para o certo item (dir ou file)
    public ArrayList<Permissions> permissions;
    public String name;
    public String path;
    public long size;
    public long createdTime;
    public long lastUpdatedTime;
    
    //Parent directory
    protected Diretorio previousDirectory;
    
    public Item(String name,String currentPath, Diretorio directory){
        this.name = name;
        this.path = currentPath + "\\" + name;
        this.previousDirectory = directory;
        this.createdTime = System.currentTimeMillis();
        this.lastUpdatedTime = System.currentTimeMillis();
        this.permissions = new ArrayList<>();
    }

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }

    public Diretorio getPreviousDirectory() {
        if(previousDirectory == null){
            return null;
        }else{
            return previousDirectory;
        }
        
    }
    
    /**
     * Shows the time at wihich the file or directory was created
     */
    public void getCreatedTime(){
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(this.createdTime);
        System.out.println(formatter.format(date));
    }
    
    /**
     * Shows the time at which the file or directory was created
     */
    public void getUpdatedTime(){
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(this.lastUpdatedTime);
        System.out.println(formatter.format(date));
    }

    /**
     * Update the lastUpdatedTime to the current time every time its called.
     */
    public void updateLastUpdatedTime() {
        
        this.lastUpdatedTime = System.currentTimeMillis();
        if(previousDirectory != null){
            previousDirectory.updateLastUpdatedTime();
        }else{
            return;
        }
        
        
    }
    
        
}
