/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package goncaloazevedo.filesystem;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Goncalo Azevedo
 */
public class Diretorio extends Item{
    
    public File diretorio;
    public ArrayList<Item> fileAndFolders;
    
    public Diretorio(String name,String currentPath, Diretorio directory) {
        
        super(name,currentPath, directory);
        this.fileAndFolders= new ArrayList<>();
        this.diretorio = new File(currentPath + "\\" + name);
        this.diretorio.mkdir();
        
    }
    
    /**
     * Adds a directory to this one
     * @param dir
     */
    public void addDiretorio(Item dir){
        this.fileAndFolders.add(dir);
    }
    
    /**
     * Adds a file to this directory
     * @param file
     */
    public void addFile(Item file){
        this.fileAndFolders.add(file);
    }
     
    /**
     * Check directories and files in the current one
     * @return
     */
    public boolean checkContents(){
         boolean hasContent = false;
         if(fileAndFolders.isEmpty()){
             return hasContent;
         }else{
            for (Item item : fileAndFolders) {
                System.out.println(item.getName());
            }
            hasContent = true;
            return hasContent;
         }
     }
     
    /**
     * Get the next directory with the specific name
     * @param dirName name of the directory to travel to in the file system
     * @return
     */
    public Diretorio getNextDirectory(String dirName){
         for (Item dir : fileAndFolders) {
             if(dir instanceof Diretorio && dir.name.equals(dirName)){
                 return (Diretorio) dir;
             }else{
                 return null;
             }
             
         }
         return null;
     }
     
    /**
     * Delete the file or folder witht he name passed with the parameter
     * @param item name of the file or directory to delete
     * @return true if it deletes, returns false if it is not possible to delete
     */
    public boolean deleteItem(String item){
        
        for (Item dir : fileAndFolders) {
             if(dir.getName().equals(item)){
                 System.out.println(item + " " + "Removed Successfully");
                 
                 deleteDirectory(getNextDirectory(item).diretorio);
                 return this.fileAndFolders.remove(dir);
                 
                 
             }
         }
         System.out.println(item + " " + "not found");
         return false;
     }
    
     //Deletes the directory in project directory (Windows)
    private boolean deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                deleteDirectory(file);
            }
        }
        return directoryToBeDeleted.delete();
    }

    // Deletes the directory in the project directory (Windows)
    boolean deleteFile(String fileToDelete) {
        
        for (Item file : fileAndFolders) {
             if(file.getName().equals(fileToDelete) && file instanceof Ficheiro){
                 Ficheiro deleteFile = (Ficheiro)file;
                 deleteFile.delete();
                 return fileAndFolders.remove(file);
             }
         }
        System.out.println("File does not exit in current directory");
        return false;
    }
    
    /**
     * Show the content of the file if it exists in the directory
     * @param fileToCheck
     */
    public void outputFileContents(String fileToCheck) {
        for (Item file : fileAndFolders) {
             if(file.getName().equals(fileToCheck) && file instanceof Ficheiro){
                 Ficheiro checkFile = (Ficheiro)file;
                 System.out.println(checkFile.getContent());
                 return;
             }
         }
        System.out.println("File does not exit in current directory");
    }
    
    /**
     * edit the content of the file if it exists in the directory
     * @param fileToEdit name of the file to edit
     * @throws IOException
     */
    public void editFileContent(String fileToEdit) throws IOException{
        for (Item file : fileAndFolders) {
             if(file.getName().equals(fileToEdit) && file instanceof Ficheiro){
                 Ficheiro editFile = (Ficheiro)file;
                 Scanner scan = new Scanner(System.in);
                 System.out.println("Current File Content:");
                 System.out.println(editFile.getContent());
                 System.out.println("New Content to replace in Current file" + file.getName());
                 String newContent = scan.nextLine();
                 editFile.setContent(newContent);
                 return;
             }else{
                 System.out.println("File does not exit in current directory");
                 return;
             }
         }
        
    }
    
    public void updateFileDate(String fileName){
        for (Item file : fileAndFolders) {
             if(file.getName().equals(fileName) && file instanceof Ficheiro){
                 Ficheiro editFile = (Ficheiro)file;
                 editFile.updateLastUpdatedTime();
                 return;
             }
         }
    }
    
    
    public void checkFileCreatedTime(String fileName) throws IOException{
        for (Item file : fileAndFolders) {
             if(file.getName().equals(fileName) && file instanceof Ficheiro){
                 file.getCreatedTime();
                 return;
             }
         }
        System.out.println("File does not exit in current directory");
    }
    
    public void checkFileUpdatedTime(String fileName) throws IOException{
        for (Item file : fileAndFolders) {
             if(file.getName().equals(fileName) && file instanceof Ficheiro){
                 file.getUpdatedTime();
                 return;
             }
         }
        System.out.println("File does not exit in current directory");
    }

    
    public Item itemCopy(String itemToCopy){
        for (Item item : fileAndFolders) {
             if(item.getName().equals(itemToCopy)){
                 return item;
             }
         }
        
        return null;
    }
    
    
    //Only used when quit is inputed until persistance is implemented
    public void deleteRootFileInExplorer(){
        diretorio.delete();
    }
}
