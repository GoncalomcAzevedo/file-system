
package goncaloazevedo.filesystem;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 *
 * @author Goncalo Azevedo
 */
public class Main {
    
    public static void main(String[] args) throws IOException {
        
        //Criar a root do file system quando o programa é compilado.
        String currentPath = Paths.get("").toAbsolutePath().toString();
        Diretorio rootDirectory = new Diretorio("root",currentPath, null);
        
        //Menu
        System.out.println("File System:");
        
        System.out.println("Type ls to check directory folders and files");
        System.out.println("Type cd and the directory to change to that directory");
        System.out.println("Type rm and the directory or file you wish to remove from the current directory");
        System.out.println("Type rmfile and the name of the file you wish to remove from the current directory");
        System.out.println("Type makedir and then the name of the new directory to create a new directory in te current one");
        System.out.println("Type makefile and then the name of the new file to create it on ther current directory");
        System.out.println("Type nano and then the name of the file in the current directory you wish to edit");
        System.out.println("Type cat and then the file name to view the contents of a file in the current directory");
        System.out.println("Type chkCreatedDate to show the created date of the current file");
        System.out.println("Type chkUpdatedDate to show the last Date the file was updated");
       
        
        //Estas operações não ficaram implementadas
        System.out.println("Type cp and then the file or directory name you wish to copy and in the end the path you wish to copy it to (Not Implemented Yet");
        System.out.println("Type mv and then the file or directory name you wish to copy and in the end the path you wish to move it to (Not Implemented Yet");
        ///////
        
        System.out.println("Type options to show this menu again");
        
        
        System.out.println("Type q to quit the filesystem");
        
        //Show initial path
        currentPath = rootDirectory.getPath();
        Diretorio currentDiretorio = rootDirectory;
        Ficheiro currentFicheiro = null;
        
        System.out.println(currentPath);
        
        Scanner scan = new Scanner(System.in);
        
        boolean quit = false;
        do {
            System.out.println(currentDiretorio.getPath());
            String choice = scan.nextLine();

            switch (choice.split(" ")[0]){
                case "ls":{
                    
                    currentDiretorio.checkContents();
                    break;
                }
                
                case "cd":{
                    
                    String DirName = choice.split(" ")[1];
                    
                    Diretorio nextDir = currentDiretorio;
                    currentDiretorio = currentDiretorio.getNextDirectory(DirName);
                    
                    if(currentDiretorio == null){
                        currentDiretorio = nextDir;
                        currentPath = currentDiretorio.getPath();
                        System.out.println("Diretorio não existe");
                        break;
                    }else{
                        currentPath = currentDiretorio.getPath();
                        System.out.println(currentDiretorio.getName());
                        
                    }
                    
                    break;
                    
                }
                
                case "cd..":{
                    
                    Diretorio prevDir = currentDiretorio;
                    currentDiretorio = currentDiretorio.getPreviousDirectory();
                    
                    if(currentDiretorio == null){
                        System.out.println("Already in file system root");
                        currentDiretorio = prevDir;
                        currentPath = currentDiretorio.getPath();
                    }else{
                        currentPath = currentDiretorio.getPath();
                        System.out.println(currentDiretorio.getName());
                    }
                    
                    break;
                    
                }
                case "makedir":{

                String newDirName = choice.split(" ")[1];

                String newDirPath = currentPath + '\\' + newDirName;
                Diretorio newDirectory = new Diretorio(newDirName, currentPath, currentDiretorio);

                currentDiretorio.addDiretorio(newDirectory);
                currentDiretorio = newDirectory;
                currentPath = newDirectory.getPath();

                break;}
                
                case "makefile":{
                    
                    String newFileName = choice.split(" ")[1];
                    
                    Ficheiro newFile = new Ficheiro("", newFileName, currentPath, currentDiretorio);
                    currentDiretorio.addFile(newFile);
                    break;
                }
                
                case "rm": {
                    
                    String itemToDelete = choice.split(" ")[1];
                    
                    
                    boolean deleted = currentDiretorio.deleteItem(itemToDelete);
                    
                    break;
                }
                
                case "rmfile": {
                    
                    String fileToDelete = choice.split(" ")[1];
                    
                    boolean deleted = currentDiretorio.deleteFile(fileToDelete);
                    
                    break;
                }
                
                case "cat": {
                    String[] checkCommand = choice.split(" ");
                    
                    if(checkCommand.length == 1){
                        System.out.println("No file inputed");
                        break;
                    }else{
                        String fileToCheck = choice.split(" ")[1];
                        currentDiretorio.outputFileContents(fileToCheck);
                    }
                    
                    break;
                }
                
                case "nano": {
                    String[] checkCommand = choice.split(" ");
                    
                    if(checkCommand.length == 1){
                        System.out.println("No file inputed");
                        break;
                    }else{
                        String fileToEdit = choice.split(" ")[1];
                        currentDiretorio.editFileContent(fileToEdit);
                        currentDiretorio.updateLastUpdatedTime();
                        currentDiretorio.updateFileDate(fileToEdit);
                    }
                    
                    break;
                }
                
                case "cp": {
//                    String[] checkCommand = choice.split(" ");
//                    
//                    if(checkCommand.length == 1 || checkCommand.length == 2){
//                        System.out.println("No file inputed or path inputed");
//                        break;
//                    }else{
//                        String itemToCopy = choice.split(" ")[1];
//                        String newPath = choice.split(" ")[2];
//                        Item copy = currentDiretorio.itemCopy(itemToCopy);
//                        
//                        if(copy == null){
//                            System.out.println("File or Directory not found");
//                            break;
//                        }else{
//                            
//                        }
//                    }
                    System.out.println("Funcion not implemented yet");
                    break;
                }
                
                case "mv": {
                    System.out.println("Function not implemented yet.");
                }
                
                case"chkCreatedDate": {
                    currentDiretorio.getCreatedTime();
                    break;
                }
                case"chkUpdatedDate": {
                    currentDiretorio.getUpdatedTime();
                    break;
                }
                
                case "options":{
                    
                    System.out.println("Type ls to check directory folders and files");
                    System.out.println("Type cd and the directory to change to that directory");
                    System.out.println("Type rm and the directory or file you wish to remove from the current directory");
                    System.out.println("Type rmfile and the name of the file you wish to remove from the current directory");
                    System.out.println("Type makedir and then the name of the new directory to create a new directory in te current one");
                    System.out.println("Type makefile and then the name of the new file to create it on ther current directory");
                    System.out.println("Type nano and then the name of the file in the current directory you wish to edit");
                    System.out.println("Type cat and then the file name to view the contents of a file in the current directory");
                    System.out.println("Type chkCreatedDate to show the created date of the current file");
                    System.out.println("Type chkUpdatedDate to show the last Date the file was updated");
                    System.out.println("Type options to show this menu again");
                    System.out.println("Type q to quit the filesystem");

                    
                    break;
                    
                }
                case "q":{
                    quit = true;
                    //call until persistence implemented
                    rootDirectory.deleteRootFileInExplorer();
                    break;
                }
                default:{
                    System.out.println("Command " + choice + " not recognized");
                }
            }
                       
        }while(quit != true);

        

 
        
    }
    
    //
    private void commonPath(String pathOld, String pathNew){
        String[] splitOld = pathOld.split("\\");
        String[] splitNew = pathNew.split("\\");
        int biggestPath;
        if(splitOld.length > splitNew.length){
            biggestPath = splitOld.length;
        }else{
            biggestPath = splitNew.length;
        }
        
        for(int i = 0; i < biggestPath; i++ ){
            
        }
        
        
    }
}
